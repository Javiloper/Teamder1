<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
Use App\Models\User;


class AuthController extends Controller
{

    public function register(Request $request) {

        $user = User::create([
            'name'          => $request->input('name'),
            'lastname'      => $request->input('lastname'),
            'username'      => $request->input('username'),
            'email'         => $request->input('email'),
            'password'      => Hash::make($request->input('password')),
            'birth_date'    => $request->input('birth_date')
        ]);

        return response()->json([
            'data' => $user
        ]);

        
    }

    public function login(Request $request) {

        if(!Auth::attempt($request->only('email', 'password'))) {
            return response([
                'message' => 'Invalid credentials'  
            ]);
        }

        $user = Auth::user(); 
        $token = $user->createToken('token')->plainTextToken;
        $cookie = cookie('jwt', $token, 60*24);

        return response(['success' => 1,
                        'token' => $token]);
                        // ->withCookie($cookie);
    }

    //
    public function user() {
        return Auth::user();
    }
}
