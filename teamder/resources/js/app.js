/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

 require('./bootstrap');

 /**
  * Next, we will create a fresh React component instance and attach it to
  * the page. Then, you may begin adding components to this application
  * or customize the JavaScript scaffolding to fit your unique needs.
  */
 
 // require('./components/Example');
 // require('./components/TrainingSeassonForm');
 
 // Create a new training
 // require('./components/FullTrainingForm');
 
 
 // Show my trainings and more data
 // require('./components/MyTrainings');
 
 // Show a specific training
 // require('./components/MyTraining');
 
 
 // require('./components/FullTrainingForm2');
 
 require('./components/App');