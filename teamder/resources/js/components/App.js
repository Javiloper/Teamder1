import React, { useContext } from 'react';
import ReactDOM from 'react-dom';


import {
    BrowserRouter,
    Routes,
    Route
} from 'react-router-dom';

import { UserContext } from './auth/userContext/UserContext';
import UserContextProvider from './auth/userContext/UserContext';

import NavBar from './NavBar';

// Auth Routes
import LogIn from './auth/login/LogIn';
import Register from './auth/register/Register';

// User Routes
import FindYourTeam from './FindYourTeam';
import MyAccount from './userPages/MyAccount/MyAccount';
import MyProjects from './userPages/MyProjects/MyProjects';


function App() {

    const { user } = useContext(UserContext); 
    console.log('user: ')
    console.log( user )
    return (
        <React.Fragment>
                
            <NavBar user={ user }/>
            <BrowserRouter>
                <Routes>
                    { user && (
                    <>
                        <Route path='/user/account' element={<MyAccount user={ user } />}></Route>
                        <Route path='/user/projects' element={<MyProjects user={ user } />}></Route>
                        <Route path='/find-your-team' element={<FindYourTeam />}></Route>
                    </>
                    )}

                    {!user && (
                    <>
                        <Route path='/login' element={<LogIn />}></Route>
                        <Route path='/register' element={<Register />}></Route>
                    
                    </>
                    )}
                    
                   
                </Routes>
            </BrowserRouter>
        </React.Fragment>
    );
}

export default App;

if (document.getElementById('root')) {
    ReactDOM.render(
        <React.StrictMode>
            <UserContextProvider>
                <App />
            </UserContextProvider>
        </React.StrictMode>
        , document.getElementById('root'));
}
