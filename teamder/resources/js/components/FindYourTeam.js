import React, {useState, useEffect} from 'react';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';

// Icons
import IconButton from '@mui/material/IconButton';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import TagFacesIcon from '@mui/icons-material/TagFaces';
import SentimentVeryDissatisfiedIcon from '@mui/icons-material/SentimentVeryDissatisfied';
// Colors
import { pink, yellow, blue, grey, green, red } from '@mui/material/colors';


import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';

function FindYourTeam() {

    const [project, setProject] = useState({

    });

    const fetchData = async () => {
        const api = await fetch('project/firstproject');
        setProject(
            await api.json()
        )
    }


    return (
        <React.Fragment>
            <Box sx={{ mx: 'auto', mt: 10, width: ['95%', '50%']}}>
                <Grid container>
                    <Grid item xs={12} ms={12}>

                        <Card sx={{ mt: 2 }}>

                            {/* Data project */}

                            <CardHeader title={project?.false_name  ? project.false_name : 'Waiting for a name'}  subheader={project?.date ? project.date : 'Waiting for a date'}>

                            </CardHeader>

                            <CardMedia
                                component="img"
                                height="300"
                                width={600}
                                image={project?.media ? project.media : "https://picsum.photos/600/400"}
                                alt="media"
                            />
                             <CardContent>
                                <Typography variant="body2" color="text.secondary">
                                {project?.description ? project.description : "Join out team as a new partner and help us create a product for the fitness sector. We are looking for 5 people really pro-active. Match with us! If we consider your profile and your ideas interesting we will select you to join our team! "}
                                {project?.link ? <Chip label="Our website" component="a" href={project.link} clickable /> : ''}
                                </Typography>
                            </CardContent>

                            {/* Buttons idk, yes, no */}

                            <CardActions sx={{ mt: 2, mb: 2}}>
                                <Grid container >
                                    <Grid item xs={3} md={4} >
                                        <IconButton sx={{ mx: 'auto', display: 'block', backgroundImage: 'linear-gradient(to right, #5bc5c1, #0c59a9)' }} aria-label="question">
                                            <QuestionMarkIcon sx={{ color: 'white', fontSize: 35 }} />
                                        </IconButton>
                                    </Grid>
                                    <Grid item xs={3} md={4}>
                                        <IconButton sx={{ mx: 'auto', display: 'block', backgroundImage: 'linear-gradient(to right, #a0e039, #18a536)' }} aria-label="yes">
                                            <CheckIcon sx={{ color: 'white', fontSize: 35 }} />
                                        </IconButton>
                                    </Grid>
                                    <Grid item xs={3} md={4}>
                                        <IconButton sx={{ mx: 'auto', display: 'block', backgroundImage: 'linear-gradient(to right, #8243a2, #ff2c2c)' }} aria-label="nop">
                                            <CloseIcon sx={{ color: 'white', fontSize: 35 }} />
                                        </IconButton> 
                                    </Grid>
                                </Grid>
                                
                                
                            </CardActions>
                        </Card>

                    </Grid>
                </Grid>
            </Box>
        </React.Fragment>
    )

}


export default FindYourTeam;