import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';

import {stringAvatar} from './avatarFunctions/AvatarFunctions';

const pages = [['Find your team', '/find-your-team']];
const settings = [['Account', '/user/account'], ['My projects', '/user/projects'], ['My  requests', '/user/request'], ['Logout', '/logout']];

const appName = 'TEAMDER';

const NavBar = (props) => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const goLogin = () => {
    window.location.href = '/login'
  }

  const goRegister = () => {
    window.location.href = '/register'
  }

  const optionsUser = (settings, props) => {
    if(props.user != null) {
      if(props.user.id > 0 && props.user.id != null) {
        return (
          <Box>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar alt={props.user.name + ' ' + props.user.lastname} {...stringAvatar(props.user.name + ' ' + props.user.lastname)} />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: '45px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
              >
              {settings.map((setting) => (
                <MenuItem key={setting[0]} onClick={handleCloseUserMenu}>
                  <Typography textAlign="center">
                    <Link href={setting[1]} className='nav-profile-link' >
                      {setting[0]}
                    </Link>
                  </Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
        );
      }
      else {
        return (
          <Box sx={{ flexGrow: 0 }}>
              <Button className='btn-login' onClick={ goLogin } variant='contained'>Log in</Button> or
              <Button className='btn-register' onClick={ goRegister } sx={{ml: 1}} variant='contained'>Join us</Button>
          </Box>
        );
      }
    }
    else {
      return null;
    }
  }

  return (
    <AppBar position="fixed" sx={{ backgroundColor: '#ff7c7c'}}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
          >
            {appName}
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page[0]} onClick={handleCloseNavMenu}>
                  <Link href={page[1]} className='my-link-nav' variant="body2">
                    <Typography textAlign="center">{page[0]}</Typography>
                  </Link>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}
          >
            {appName}
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {pages.map((page) => (
              <Button
                key={page[0]}
                onClick={handleCloseNavMenu}
                sx={{ my: 2, color: 'white', display: 'block' }}
              >
                  <Link href={page[1]} className='my-link-nav' >
                    <Typography textAlign="center">{page[0]}</Typography>
                  </Link>
              </Button>
            ))}
          </Box>

          { optionsUser(settings, props) }
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default NavBar;
