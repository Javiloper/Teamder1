import React, {useState, useContext} from 'react';

import { UserContext } from './../userContext/UserContext';


import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

function LogIn() {

    const {loginUser, wait, loggedInCheck} = useContext(UserContext);

    const [redirect, setRedirect] = useState(false);

    const [dataForm, setDataForm] = useState({
        email: '',
        password: '',
    })

    const handleChange = (e) => {
        
        setDataForm({
            ...dataForm,
            [e.target.name]: e.target.value
        })
        console.log(dataForm)

    }

    const handleSubmit = async (e) => {

        console.log(dataForm)

        const data = await loginUser(dataForm);
        if(data.success){
            console.log('Log in done')
            setRedirect('Redirecting...');
            await loggedInCheck();
            return;
        }

    }

    return (
        <React.Fragment>
            <Box sx={{ mx: 'auto', mt: 2, width: ['95%', '50%']}}>
                <Grid container>
                    <Grid item xs={12} ms={12}>
                    <Typography variant='h4'>Log in</Typography>
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='email' fullWidth id="outlined-basic" type='text' label="Username or mail" variant="outlined" />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='password' fullWidth id="outlined-basic" type='password' label="Password" variant="outlined" />
                    <Button className='btn-login' onClick={handleSubmit} sx={{ mt: 2 }} variant='contained'>Enter</Button>
                    </Grid>
                </Grid>
            </Box>
        </React.Fragment>
    );

}

export default LogIn;