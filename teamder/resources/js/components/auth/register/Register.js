import React, { useContext, useState } from 'react';
import { UserContext } from '../userContext/UserContext';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

function Register() {

    const {registerUser, wait} = useContext(UserContext);

    const [dataForm, setDataForm] = useState({
        username: '',
        name: '',
        lastname: '',
        birth_date: '',
        email: '',
        password: '',
        // politics: 0
    });

    const handleChange = (e) => {
        
        setDataForm({
            ...dataForm,
            [e.target.name]: e.target.value
        })
        console.log(dataForm)


    }

    const handleSubmit = async (e) => {

        console.log(dataForm)

        const data = await registerUser(dataForm);
        if(data.success){
            e.target.reset();
            console.log('You have successfully registered.');
            // setErrMsg(false);
        }


        // axios.post('/auth/register', data)
        // .then(function(response){

        //     console.log(response);

        // })
        // .catch(error => {

        // })

    }

    return (
        <React.Fragment>
            <Box sx={{ mx: 'auto', mt: 2, width: ['95%', '50%']}}>
                <Grid container>
                    <Grid item xs={12} ms={12}>
                    <Typography variant='h4'>Sign Up</Typography>
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='username' fullWidth type='text' label="Username" variant="outlined" />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='name' fullWidth type='text' label="Name" variant="outlined" />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='lastname' fullWidth type='text' label="Lastname" variant="outlined" />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='birth_date'  InputLabelProps={{ shrink: true }} label='Birth date' fullWidth type='date' />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='email' fullWidth type='text' label="e-mail" variant="outlined" />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='password' fullWidth type='password' label="Password" variant="outlined" />
                    <FormGroup>
                        <FormControlLabel control={<Checkbox onChange={handleChange} name='politics' />} label="I accept politics" />
                    </FormGroup>
                    <Button onClick={handleSubmit} className='btn-login' sx={{ mt: 2 }} variant='contained'>Save</Button>
                    </Grid>
                </Grid>
            </Box>
        </React.Fragment>
    );

}

export default Register;