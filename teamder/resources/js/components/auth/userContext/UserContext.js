import {createContext, useState, useEffect} from 'react'
import axios from 'axios'

export const UserContext = createContext();

export const UserContextProvider = ({children}) => {

    const [theUser, setTheUser] = useState(null);
    const [wait, setWait] = useState(false);

    const registerUser = async ({ username, name, lastname, birth_date, email, password}) => {
        setWait(true);
        try{
            const {data} = await axios.post('/auth/register',{
                username,
                name,
                lastname,
                birth_date,
                email,
                password, 
            });
            setWait(false);
            return data;
        }
        catch(err){
            setWait(false);
            return {success:0, message:'Server Error!'};
        }
    }

    const loginUser = async ({email,password}) => {
        setWait(true);
        try{
            const {data} = await axios.post('/auth/login',{
                email,
                password 
            });

            console.log(data)

            if(data.success && data.token){
                localStorage.setItem('loginToken', data.token);
                setWait(false);
                return {success:1};
            }
            setWait(false);
            return {success:0, message:data.message};
        }
        catch(err){
            setWait(false);
            return {success:0, message:'Server Error!'};
        }

    }

    const loggedInCheck = async () => {
        const loginToken = localStorage.getItem('loginToken');
        axios.defaults.headers.common['Authorization'] = 'Bearer '+loginToken;
        console.log('loginToken:' + loginToken)
        if(loginToken){
            await fetch('/auth/user')
                .then(res => res.json())
                .then(
                    json => {
                        setTheUser( json )
                        
                    } 
                )
            
            // console.log('theUser')
            // console.log(theUser)
            // if(data.id > 0){
            //     setTheUser(data.id);
            //     setTimeout(() => {
            //         console.log('LoggedInCheck->' + theUser)
            //     }, 1000);
            //     console.log(data.id)
            //     return;
            // }
            // setTheUser(null);
        }
    }

    useEffect(() => {
        async function asyncCall(){
            await loggedInCheck();
        }
        asyncCall();
    },[]);

    const logout = () => {
        localStorage.removeItem('loginToken');
        setTheUser(null);
    }

    return (
        <UserContext.Provider value={{registerUser,loginUser,wait, user:theUser, loggedInCheck,logout}}>
            {children}
        </UserContext.Provider>
    );

}

export default UserContextProvider;