import React, {useState} from 'react';

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';


function MyAccount(props) {

    const userData = () => {
        if(props.user != null) {
            return props.user;
        }
        else { 
            return 'waiting...'; 
        }
    }

    const handleChange = () => {

    }


    const handleSubmit = () => {
        
    }

    return(
        <React.Fragment>
            <Box sx={{ mx: 'auto', mt: 2, width: ['95%', '50%']}}>
                <Grid container>
                    <Grid item xs={12} ms={12}>
                        <Paper elevation={3} sx={{pt: 1, pb: 2}}>
                            <Typography variant='h4' sx={{ml: 1, mt: 1}}>My Account</Typography>
                            <div class='my-account-data'>
                                <p>Name: {userData().name}</p>
                                <p>Lastname: {userData().lastname}</p>
                                <p>Birth Date: {userData().birth_date}</p>
                                <p>E-mail: {userData().email}</p>
                            </div>
                        </Paper>

                    </Grid>
                </Grid>
            </Box>            
        </React.Fragment>
    );

}

export default MyAccount;