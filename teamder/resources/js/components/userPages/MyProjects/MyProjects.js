import React, { useState } from 'react';

import { styled } from '@mui/material/styles';

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';


import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';

import IconButton from '@mui/material/IconButton';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import ImageIcon from '@mui/icons-material/Image';
import axios from 'axios';

function MyProjects(props) {

    const Input = styled('input')({
        display: 'none',
    });

    // Project data 
    const [project, setProject] = useState({
        name: '',
        false_name: '',
        description: '',
        long_description: '',
        link: '',
        files: [],
    })


    // True when we click on "New Project"
    const [activeProject, setActiveProject] = useState(false)

    // Update project data when change each input
    const handleChange = (e) => {
        
        if(e.target.name != 'files') {
            setProject({
                ...project,
                [e.target.name]: e.target.value
            })
        }
        else {
            let files = project.files;
            files.push(e.target.value);
            setProject({
                ...project,
                [e.target.name]: files
            })
        }
        console.log(project)
    }

    const handleCreateProject = () => {

        axios.post('/project/new', project)
        .then((response) => {
            console.log(response)
        })
    }


    // Render this when the param active is true (setting activeProject)
    const startNewProject = (active) => {
        
        if(active) {
            return (
                
                <Grid container sx={{ mt: 5, mb: 5 }}>
                <Grid item xs={12} md={12}>
                    <Typography variant='h4' sx={{textAlign: 'center'}}>NEW PROJECT</Typography>
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='name' fullWidth type='text' label="Title" variant="outlined" />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='false_name' fullWidth type='text'  label="Visible Title" variant="outlined" />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='description' fullWidth type='text' label="Description" multiline minRows={2} maxRows={6} variant="outlined" />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='long_description' fullWidth type='text' label="Long Description" multiline minRows={2} maxRows={12} variant="outlined" />
                    <TextField sx={{ mt: 2 }} onChange={handleChange} name='link' fullWidth type='text' label="Link" variant="outlined" />

                    {/* <div className='thick-blue-border normal-round-border' style={{marginTop: '2vh', padding: '1vh'}}>  */}

                    <Typography variant='h6' className='color-blue' sx={{mt: 2, display: 'block'}}>Do you need some media for your project? Upload images, audio-files and videos!</Typography>
                    
                    <label htmlFor="icon-button-file" >
                        <Input onChange={handleChange} id="icon-button-file" type="file" name='files'/>
                        <IconButton size='large' color="primary" aria-label="upload picture" component="span" className='color-blue' sx={{mt: 2}}>
                            <FileUploadIcon />
                        </IconButton>
                    </label>
                    {
                        project?.files ?
                        project?.files?.map((file) => (
                            <p style={{marginLeft: '1vh'}} className='color-blue'><ImageIcon />{ file }</p>
                            )) : ''
                    }
                    {/* </div> */}

                    
                    <Button onClick={handleCreateProject} variant='contained' className='btn-block btn-contained-green' sx={{ mt: 2 }}>Create</Button>
                </Grid>
            </Grid>
            );
        }
        else{ 
            return null;
        }

    }

    return (
        <React.Fragment>
            <Box sx={{ mx: 'auto', mt: 10, width: ['95%', '50%']}}>
                <Grid container>
                    <Grid item xs={12} md={12}>
                        <Typography variant='h3' sx={{textAlign: 'center'}}>Welcome to My Projects!</Typography>
                        
                        <Grid container sx={{mt: 3}}>
                            <Grid item xs={12} md={5}>
                                <Card sx={{height: '150px'}}>
                                    <CardContent>
                                     Did you start any project? Try and surprise our public with your idea!
                                    <Button onClick={ () => setActiveProject(true) } variant='contained' sx={{mt: 2}} className='btn-block btn-contained-green'>New Project</Button>
                                    </CardContent>
                                </Card>
                            </Grid>
                            <Grid item md={1} />
                            <Grid item xs={12} md={5}>
                                <Card sx={{height: '150px'}}>
                                    <CardContent>
                                     Do you want to open your old projects? Check how it is going!
                                    <Button variant='outlined' sx={{mt: 2}} className='btn-block'> Open my projects</Button>
                                    </CardContent>
                                </Card>
                            </Grid>
                        </Grid>

                        { startNewProject(activeProject) }
                       
                    </Grid>
                </Grid>
            </Box>
        </React.Fragment>

    )
}

export default MyProjects;